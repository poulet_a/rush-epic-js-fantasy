/*
** server.h for  in /home/poulet_a/projets/rushfantasy/server
**
** Made by poulet_a
** Login   <poulet_a@epitech.net>
**
** Started on  Sat May 10 09:50:39 2014 poulet_a
** Last update Sat May 10 17:09:38 2014 stefan-l
*/

#ifndef SERVER_H_
# define SERVER_H_

# include "structs.h"

# define ERR_LOAD_MAP	-42
# define GAME_OVER	1

/* server management */
int		run_server(int server_fd, char *map_name);

/* room */
char		room_is_empty(t_room *room);

/* champ */
char		champ_are_dead(register t_champ *champ);
char		champ_are_ready(t_champ *champ);
t_champ		*champ_first(register t_champ *champ);
int		champ_size(register t_champ *champ);

t_champ		*champ_find(t_champ *list, char *name);
t_champ		*champ_last(t_champ *list);
t_champ		*champ_create(t_champ *list, t_champ *champion);

/*
**utils
*/
int	my_getnbr(char *str);

#endif /* !SERVER_H_ */
