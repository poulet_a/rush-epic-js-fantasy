/*
** structs.h for rpg in /home/harnay_a/rendu/CPE_2013_Rush2/structs
**
** Made by harnay_a
** Login   <harnay_a@epitech.net>
**
** Started on  Sat May 10 00:04:18 2014 harnay_a
** Last update Sun May 11 21:59:40 2014 stefan-l
*/

#ifndef STRUCTS_H_
# define STRUCTS_H_

#include <sys/types.h>

# define ROOM_START	1
# define ROOM_END	2
# define INIT_NONE	0
# define INIT_MONSTER	1
# define INIT_CHAMPION	2

typedef struct		s_champ
{
  int			id;
  char			*name;
  char			*type;
  struct sockaddr	*addr;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  char			*weapon;
  char			*armor;
  char			is_ready;
  struct s_champ	*next;
}			t_champ;

typedef struct		s_monster
{
  int			id;
  char			*type;
  int			hp;
  int			spe;
  int			speed;
  int			deg;
  char			*weapon;
  char			*armor;
  struct s_monster	*next;
}			t_monster;

typedef struct	s_room
{
  char			*name;
  char			*adv;
  char			*connections;
  char			*monsters;
  t_monster		**monsters_tab;
  struct s_room		**connections_tab;
  char			position;
  struct s_room		*next;
}			t_room;

typedef struct		s_header
{
  char			*name;
  char			*start;
  char			*end;
}			t_header;

#endif /* !STRUCTS_H_ */
