/*
** client.h for rpg in /home/harnay_a/rendu/CPE_2013_Rush2/client
** 
** Made by harnay_a
** Login   <harnay_a@epitech.net>
** 
** Started on  Sat May 10 00:04:18 2014 harnay_a
** Last update Sat May 10 00:38:21 2014 poulet_a
*/

#ifndef CLIENT_H_
# define CLIENT_H_

# include "structs.h"

void		print_next(char *room_name);
void		print_list_team(t_champ *champ);
void		print_list_monster(t_monster *monster);
void		print_attack(int monster_id, int monster_hp);
void		print_attack_spe(int champ_id);

#endif /* !CLIENT_H_ */
