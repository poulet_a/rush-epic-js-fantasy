/*
** trans_tab.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat May 10 00:35:27 2014 ghukas_g
** Last update Sun May 11 21:14:04 2014 ghukas_g
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<errno.h>

static int	count_nb_tab(char *str)
{
  int		i;
  int		op;
  int		cl;

  i = 0;
  op = 0;
  cl = 0;
  while (str[i] != '\0')
    {
      op = str[i] == '[' ? op + 1 : op;
      cl = str[i] == ']' ? cl + 1 : cl;
      i = i + 1;
    }
  if (op == cl && op > 0)
    return (op);
  printf("Bad format of input\n");
  exit(EXIT_FAILURE);
}

char		**trans_tab(char *str)
{
  int		size;
  char		**tab;
  int		i;
  int		j;

  if (str == NULL)
    return (NULL);
  size = count_nb_tab(str);
  if ((tab = malloc(sizeof(char *) * (size + 1))) == NULL)
    perror("Malloc error");
  i = 0;
  j = 0;
  while (str[i] != 0)
    {
      if (str[i] == '[')
	tab[j++] = &str[i + 1];
      else if (str[i] == ']')
	str[i] = '\0';
      i = i + 1;
    }
  tab[j] = NULL;
  return (tab);
}
