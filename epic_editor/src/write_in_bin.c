/*
** write_in_bin.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat May 10 02:15:47 2014 ghukas_g
** Last update Sat May 10 03:09:15 2014 ghukas_g
*/

#include	<stdio.h>
#include	<string.h>
#include	<stdlib.h>
#include	"editor.h"

int		write_with_arg(char *flag, char *str, int fd)
{
  my_putfnbr(find_id_tab(flag), fd);
  if (strcmp(str, "None") == 0)
    {
      my_putfnbr(4, fd);
      my_putfstr("NONE", fd);
    }
  else
    {
      my_putfnbr(strlen(str), fd);
      my_putfstr(str, fd);
    }
}

int		write_champ(char **tab, int fd)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    i = i + 1;
  if (i != 9)
    exit_error("Bad format of a champion\n");
  my_putfnbr(find_id_tab("CHAMPION"), fd);
  write_with_arg("NAME", tab[1], fd);
  write_with_arg("TYPE", tab[2], fd);
  write_with_arg("HP", tab[3], fd);
  write_with_arg("SPE", tab[4], fd);
  write_with_arg("SPEED", tab[5], fd);
  write_with_arg("DEG", tab[6], fd);
  write_with_arg("WEAPON", tab[7], fd);
  write_with_arg("ARMOR", tab[8], fd);
}

int		write_monster(char **tab, int fd)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    i = i + 1;
  if (i != 8)
    exit_error("Bad format of a champion\n");
  my_putfnbr(find_id_tab("MONSTER"), fd);
  write_with_arg("TYPE", tab[1], fd);
  write_with_arg("HP", tab[2], fd);
  write_with_arg("SPE", tab[3], fd);
  write_with_arg("SPEED", tab[4], fd);
  write_with_arg("DEG", tab[5], fd);
  write_with_arg("WEAPON", tab[6], fd);
  write_with_arg("ARMOR", tab[7], fd);
}

int		write_room(char **tab, int fd)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    i = i + 1;
  if (i != 5)
    exit_error("Bad format of a champion\n");
  my_putfnbr(find_id_tab("ROOM"), fd);
  write_with_arg("NAME", tab[1], fd);
  write_with_arg("ADV", tab[2], fd);
  write_with_arg("TAB=>CONNECTION", tab[3], fd);
  write_with_arg("TAB=>MONSTER", tab[4], fd);
}
