/*
** fput.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat May 10 00:43:39 2014 ghukas_g
** Last update Sun May 11 21:20:40 2014 ghukas_g
*/

#include	<unistd.h>
#include	<string.h>

void	my_putfchar(char c, int fd)
{
  write(fd, &c, 1);
}

void	my_putfstr(char *str, int fd)
{
  write(fd, str, strlen(str));
}

void	my_putfnbr(int size, int fd)
{
  static count = 0;

  if (size != -1)
    {
      ++count;
      if (count < 1)
	my_putfnbr(size / 256, fd);
      my_putfchar(size % 256, fd);
    }
}
