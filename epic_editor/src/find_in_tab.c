/*
** find_in_tab.c for  in /home/ghukas_g/rendu/colle/rush-epic-js-fantasy/epic_editor
**
** Made by ghukas_g
** Login   <ghukas_g@epitech.net>
**
** Started on  Sat May 10 00:45:13 2014 ghukas_g
** Last update Sat May 10 00:53:02 2014 ghukas_g
*/

#include	<stdio.h>
#include	<string.h>
#include	"editor.h"

int		find_nb_tab(char *name)
{
  int		i;

  i = 0;
  while (i < 20)
    {
      if (strcmp(g_tab[i].name, name) == 0)
	return (i);
      i = i + 1;
    }
  return (-1);
}

int		find_id_tab(char *name)
{
  int		i;

  i = 0;
  while (i < 20)
    {
      if (strcmp(g_tab[i].name, name) == 0)
	return (g_tab[i].id);
      i = i + 1;
    }
  return (-1);
}
