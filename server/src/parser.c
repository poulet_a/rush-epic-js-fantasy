/*
** recup_infos.c for rush in /home/stefan_l/rendu/rush-epic-js-fantasy
**
** Made by stefan-l
** Login   <stefan_l@epitech.net>
**
** Started on  Fri May  9 23:48:38 2014 stefan-l
** Last update Sun May 11 23:01:39 2014 stefan-l
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "server.h"
#include "get_next_line.h"
#include "structs.h"

int		fill_room(t_room **room, char *str, int *count, t_header *datas)
{
  char		**tmp;
  t_room	*elem;

  if ((elem = malloc(sizeof(t_room))) == NULL)
    return (-1);
  while (++(*count) < strlen(str) && str[*count] != 0xA)
    {
      tmp = (str[*count] == 0x1) ? &(elem->name) :
	((str[*count] == 0x10) ? &(elem->adv) :
	 ((str[*count] == 0x11) ? &(elem->connections) :
	  ((str[*count] == 0x12 ? &(elem->monsters) : NULL))));
      if ((*tmp = strndup(&(str[*count + 2]), str[*count + 1])) == NULL)
	return (-1);
      if (tmp != NULL)
	*count = 1 + str[*count + 1] + *count;
    }
  elem->next = NULL;
  if (*room == NULL)
    *room = elem;
  else
    {
      (*room)->next = elem;
      *room = (*room)->next;
    }
  return (0);
}

int		which_funct_to_send(char *str,
				   t_champ **champs,
				   t_monster **monsters,
				   t_room **room)
{
  int		count;
  int		header_here;
  t_header	datas;
  int		ret;

  count = 0;
  ret = 0;
  header_here = 0;
  while (str[++count] != 0)
    {
      if (str[count] == 0xD && header_here != 1 && (header_here = 1))
	ret = fill_header(&datas, str, &count);
      else if (str[count] == 0xC)
	ret = fill_champ(champs, str, &count);
      else if (str[count] == 0xE)
	ret = fill_monster(monsters, str, &count);
      else if (str[count] == 0xF)
	ret = fill_room(room, str, &count, &datas);
      else
	fprintf(stderr,
		"bad binary syntax : unknown data type, or double header\n");
      if (ret == -1)
	return (-1);
    }
  return (0);
}

int		fill_structs(t_champ **champs,
			     t_monster **monsters,
			     t_room **room,
			     char *str)
{
  if (strlen(str) > 1 && str[0] == 0x7b)
    {
      if (which_funct_to_send(str, champs, monsters, room) == -1)
	return (-1);
    }
  else
    {
      fprintf(stderr, "bad header, no magic number");
      return (-1);
    }
  return (0);
}

void		print_yolo(t_champ **champs)
{
  printf("chevalier %s, grand %s, vêtu de %s et equipé de %s,");
  printf("possède %d de santé, et %d de pouvoir spirituel\n",
	 (*champs)->name, (*champs)->type, (*champs)->armor,
	 (*champs)->weapon, (*champs)->hp, (*champs)->spe);
}

int		load_map(t_champ **champs, t_room **rooms, char *name)
{
  int		fd;
  t_monster	*monsters;
  char		*save;
  char		*str;

  monsters = NULL;
  save = NULL;
  fd = open(name, S_IRUSR);
  save = NULL;
  while ((str = get_next_line(fd)) != NULL)
    {
      str[strlen(str) + 1] = '\0';
      str[strlen(str)] = '\n';
      if (save != NULL)
	{
	  if ((save = realloc(save, (strlen(save) + strlen(str)) + 1)) == NULL)
	    return (-1);
	  save = strcat(save, str);
	}
      else
	save = strdup(str);
    }
  fill_structs(champs, &monsters, rooms, save);
  print_yolo(champs);
  return (0);
}
