/*
** monster.c for  in /home/poulet_a/projets/jsfantasy
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 01:48:21 2014 poulet_a
** Last update Sun May 11 22:54:36 2014 poulet_a
*/

#include <stdlib.h>
#include "structs.h"

char			room_is_empty(t_room *room)
{
  register int		i;

  if (room == NULL)
    return (1);
  if ((room->monsters) == NULL)
    return (1);
  i = 0;
  while (room->monsters_tab[i] != NULL)
    {
      if (room->monsters_tab[i]->hp > 0)
	return (0);
      ++i;
    }
  return (1);
}
