/*
** game.c for  in /home/poulet_a/projets/rushfantasy/server
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 12:42:37 2014 poulet_a
** Last update Sun May 11 22:53:59 2014 poulet_a
*/

#include <stdlib.h>
#include "structs.h"
#include "server.h"

int	play_monster(t_monster *monster,
		     t_champ *champ,
		     t_room *room)
{
  if (champ->hp > 0)
    champ->hp -= monster->deg;
  printf("%s attacks you and inflict %d dammages ! You fall to %d !\n",
	 monster->type, monster->deg, champ->hp);
  return (0);
}

int	play_champ(t_champ *champ,
		   t_room *room)
{
  char	**cmd;

  if ((cmd = get_cmd()) == NULL)
    return (ERR_MALLOC);
  return (0);
}

static char	get_initiative(t_champ *champion,
			       t_monster **monsters)
{
  return ((get_max_speed(monsters) <= champion->spe)
	  ? (INIT_CHAMPION) : (INIT_MONSTER));
}

int		fight(t_champ *champ, t_room *room)
{
  char		init;
  int		i;

  init = get_initiative(champ, room->monsters_tab);
  i = 0;
  if (init == INIT_CHAMPION)
    play_champ(champ, room);
  while (room->monsters[i] != NULL)
    {
      play_monster(room->monsters_tab[i], champ, room);
      ++i;
    }
  if (init != INIT_CHAMPION)
    play_champ(champ, room);
  return (0);
}
