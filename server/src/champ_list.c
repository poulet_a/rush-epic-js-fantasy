/*
** champ_list.c for  in /home/poulet_a/projets/rushfantasy/server
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sat May 10 12:45:07 2014 poulet_a
** Last update Sat May 10 15:20:57 2014 poulet_a
*/

#include <stdlib.h>
#include "structs.h"

t_champ		*champ_find(t_champ *list, char *name)
{
  if (list == NULL)
    return (NULL);
  while (list != NULL)
    {
      if (my_match(list->name, name))
	return (list);
      list = list->next;
    }
  return (list);
}

static t_champ	*champ_new(t_champ *champion, t_champ *next)
{
  t_champ	*list;

  if ((list = malloc(sizeof(t_champ))) == NULL)
    return (NULL);
  *list = *champion;
  memcpy(champion, list, sizeof(t_champ));
  list->next = next;
  return (list);
}

t_champ		*champ_last(t_champ *list)
{
  t_champ	*tmp;

  if ((tmp = list) == NULL)
    return (NULL);
  while (tmp->next != NULL)
    {
      tmp = tmp->next;
    }
  return (tmp);
}

t_champ		*champ_create(t_champ *list, t_champ *champion)
{
  t_champ	*tmp;

  tmp = champ_last(list);
  if (tmp == NULL)
    return (list = champ_new(champion, NULL));
  tmp->next = champ_new(champion, NULL);
  return (list);
}
